package com.sts.web.acts;
import com.sts.domain.acts.Acts;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.roo.addon.web.mvc.controller.finder.RooWebFinder;

@RequestMapping("/actses")
@Controller
@RooWebScaffold(path = "actses", formBackingObject = Acts.class)
@RooWebFinder
public class ActsController {
}
