package com.sts.repository.acts;
import com.sts.domain.acts.Acts;
import org.springframework.roo.addon.layers.repository.jpa.RooJpaRepository;

@RooJpaRepository(domainType = Acts.class)
public interface ActsRepository {
}
