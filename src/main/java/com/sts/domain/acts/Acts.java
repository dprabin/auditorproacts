package com.sts.domain.acts;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.entity.RooJpaEntity;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaEntity(table = "acts")
@RooSerializable
public class Acts implements IActs {

    /**
	 * Serializable needs UID which this class extends fromITD
	 */
	private static final long serialVersionUID = 4617796547207963598L;

	/**
     */
	 @javax.persistence.Id
	 @GeneratedValue(strategy = GenerationType.AUTO)
	 @Column(name = "actid")
	 private Integer actId;

    /**
     */
    @NotNull
    @Size(min = 1, max = 20)
    private String act;

    /**
     */
    @NotNull
    private String chapter;

    /**
     */
    private String section_no;

    /**
     */
    private String sub_section_no;

    /**
     */
    private String sub_sub_section_no;

    /**
     */
    @NotNull
    private String type;

    /**
     */
    @NotNull
    @Size(max = 1000)
    private String field_value;
}
