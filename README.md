Auditor Pro Acts and Rules. 
Language: Java
Framework: Spring

git clone https://dprabin@bitbucket.org/dprabin/auditorproacts.git

open from STS

then do maven>update project

open mysql and create a db named acts

import src/main/resources/acts.sql to acts table

change src/main/resources/META-INF/spring/database.properties file for your db username and password. Also change mysql port no (maybe to default 3306) localhost:3306

run the project in tomcat/jetty/vfabric server
